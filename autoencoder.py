# -*- coding: utf-8 -*-

""" Auto Encoder Example.
Using an auto encoder on MNIST handwritten digits.
References:
    Y. LeCun, L. Bottou, Y. Bengio, and P. Haffner. "Gradient-based
    learning applied to document recognition." Proceedings of the IEEE,
    86(11):2278-2324, November 1998.
Links:
    [MNIST Dataset] http://yann.lecun.com/exdb/mnist/
"""
from __future__ import division, print_function, absolute_import

import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import scipy.io as sio
import pdb
from Queue import Queue
# Import MNIST data
# from tensorflow.examples.tutorials.mnist import input_data
# import pdb; pdb.set_trace()
# mnist = input_data.read_data_sets("MNIST_data", one_hot=True)

data = sio.loadmat('taylorGreen4VesVC10.mat')
posx = data['posx']
posy = data['posy']

xn = posx.reshape(64,80004).T
yn = posy.reshape(64,80004).T

for n in range(len(yn)):
    xn[n, :] = (xn[n, :] - np.mean(xn[n, :]))/np.max(xn[n, :] - np.mean(xn[n, :]))
    yn[n, :] = (yn[n, :] - np.mean(yn[n, :]))/np.max(yn[n, :] - np.mean(yn[n, :]))
    coords = np.vstack([xn[n, :], yn[n, :]])
    cov = np.cov(coords)
    evals, evecs = np.linalg.eig(cov)
    sort_indices = np.argsort(evals)[::-1]
    evec1, evec2 = evecs[:, sort_indices]
    x_v1, y_v1 = evec1  # Eigenvector with largest eigenvalue
    x_v2, y_v2 = evec2
    theta = np.tanh((x_v1)/(y_v1))  
    rotation_mat = np.matrix([[np.cos(theta), -np.sin(theta)],
                              [np.sin(theta), np.cos(theta)]])
    transformed_mat = rotation_mat * coords
    xn[n, :], yn[n, :] = transformed_mat.A
    ixmax = np.argmax(xn[n, :])
    xn[n, :] = np.append(xn[n, ixmax:], xn[n, :ixmax])   
    yn[n, :] = np.append(yn[n, ixmax:], yn[n, :ixmax])

inp = np.append(xn,yn,axis=1)

train = inp[10000:70000,:]
test = inp[70000:, :]
# Parameters
global_step = tf.Variable(0, trainable=False)
starter_learning_rate = 0.05
#learning_rate = tf.train.exponential_decay(starter_learning_rate, global_step,
#                                           1000, 0.0001, staircase=True)
learning_rate = 0.0025
training_epochs = 20
batch_size = 256
display_step = 1
examples_to_show = 10

# Network Parameters
n_hidden_1 = 128  # 1st layer num features
n_hidden_2 = 128  # 2nd layer num features
n_hidden_3 = 128  # 3nd layer num features
n_hidden_4 = 48   # 3nd layer num features
n_input = 128 # MNIST data input (img shape: 28*28)

# tf Graph input (only pictures)
X = tf.placeholder("float", [None, n_input])

weights = {
    'encoder_h1': tf.Variable(tf.random_normal([n_input, n_hidden_1])),
    'encoder_h2': tf.Variable(tf.random_normal([n_hidden_1, n_hidden_2])),
    'encoder_h3': tf.Variable(tf.random_normal([n_hidden_2, n_hidden_3])),
    'encoder_h4': tf.Variable(tf.random_normal([n_hidden_3, n_hidden_4])),
    'decoder_h1': tf.Variable(tf.random_normal([n_hidden_4, n_hidden_3])),
    'decoder_h2': tf.Variable(tf.random_normal([n_hidden_3, n_hidden_2])),
    'decoder_h3': tf.Variable(tf.random_normal([n_hidden_2, n_hidden_1])),
    'decoder_h4': tf.Variable(tf.random_normal([n_hidden_1, n_input])),
}
biases = {
    'encoder_b1': tf.Variable(tf.random_normal([n_hidden_1])),
    'encoder_b2': tf.Variable(tf.random_normal([n_hidden_2])),
    'encoder_b3': tf.Variable(tf.random_normal([n_hidden_3])),
    'encoder_b4': tf.Variable(tf.random_normal([n_hidden_4])),
    'decoder_b1': tf.Variable(tf.random_normal([n_hidden_3])),
    'decoder_b2': tf.Variable(tf.random_normal([n_hidden_2])),
    'decoder_b3': tf.Variable(tf.random_normal([n_hidden_1])),
    'decoder_b4': tf.Variable(tf.random_normal([n_input])),
}


# Building the encoder
def encoder(x):
    # Encoder Hidden layer with sigmoid activation #1
    layer_1 = tf.nn.sigmoid(tf.add(tf.matmul(x, weights['encoder_h1']),
                                   biases['encoder_b1']))
    # Decoder Hidden layer with sigmoid activation #2
    layer_2 = tf.nn.sigmoid(tf.add(tf.matmul(layer_1, weights['encoder_h2']),
                                   biases['encoder_b2']))
    layer_3 = tf.nn.sigmoid(tf.add(tf.matmul(layer_2, weights['encoder_h3']),
                                   biases['encoder_b3']))
    layer_4 = tf.nn.sigmoid(tf.add(tf.matmul(layer_3, weights['encoder_h4']),
                                   biases['encoder_b4']))
    return layer_4


# Building the decoder
def decoder(x):
    # Encoder Hidden layer with sigmoid activation #1
    layer_1 = tf.nn.sigmoid(tf.add(tf.matmul(x, weights['decoder_h1']),
                                   biases['decoder_b1']))
    # Decoder Hidden layer with sigmoid activation #2
    layer_2 = tf.nn.sigmoid(tf.add(tf.matmul(layer_1, weights['decoder_h2']),
                                   biases['decoder_b2']))
    layer_3 = tf.nn.sigmoid(tf.add(tf.matmul(layer_2, weights['decoder_h3']),
                                   biases['decoder_b3']))
    layer_4 = tf.nn.sigmoid(tf.add(tf.matmul(layer_3, weights['decoder_h4']),
                                   biases['decoder_b4']))
    return layer_4

# Construct model
encoder_op = encoder(X)
decoder_op = decoder(encoder_op)

# Prediction
y_pred = decoder_op
# Targets (Labels) are the input data.
y_true = X

# Define loss and optimizer, minimize the squared error

regulariser = tf.add_n((tf.nn.l2_loss(weights['encoder_h1']), tf.nn.l2_loss(weights['encoder_h2']),
                        tf.nn.l2_loss(weights['encoder_h3']), tf.nn.l2_loss(weights['encoder_h4']),
                        tf.nn.l2_loss(weights['decoder_h1']), tf.nn.l2_loss(weights['decoder_h2']),
                        tf.nn.l2_loss(weights['decoder_h3']), tf.nn.l2_loss(weights['decoder_h4'])))

cost = tf.reduce_mean(tf.pow(y_true - y_pred, 2) + 0.00001 * regulariser)
optimizer = tf.train.RMSPropOptimizer(learning_rate).minimize(cost)

# Initializing the variables
init = tf.global_variables_initializer()

# Launch the graph
with tf.Session() as sess:
    sess.run(init)
    # Training cycle
    for epoch in xrange(training_epochs):
        # Loop over all batches
        batch_xs = train
        # Run optimization op (backprop) and cost op (to get loss value)
        _, c = sess.run([optimizer, cost], feed_dict={X: batch_xs})
        # Display logs per epoch step
        if epoch % 5 == 0:
            print("Epoch:", '%04d' % (epoch+1),
                  "cost=", "{:.9f}".format(c))
    print("Optimization Finished!")
    y_predict = sess.run(y_pred, feed_dict={X: train}) 
    tosave = {'y_act':train, 'y_pred':y_predict}
    sio.savemat('train_results.mat',tosave)
    
    y_predict = sess.run(y_pred, feed_dict={X: test}) 
    print(y_pred)
    tosave = {'y_act':test, 'y_pred':y_predict}
    sio.savemat('result.mat', tosave)

